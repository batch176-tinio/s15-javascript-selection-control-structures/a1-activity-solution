
//------------------------------
console.log("Hello World");

//------------------------------
let num1 = parseInt(prompt(`Enter first number:`));
let num2 = parseInt(prompt(`Enter second number:`));

function sumOfTwoNumbers(sumNum1, sumNum2) {
	let sum = sumNum1 + sumNum2;
	return `${sumNum1} plus ${sumNum2} is equal to ${sum}.`
}

function differenceOfTwoNumbers(differenceNum1, differenceNum2) {
	let difference = differenceNum1 - differenceNum2;
	return `${differenceNum1} minus ${differenceNum2} is equal to ${difference}.`
}

function productOfTwoNumbers(productNum1, productNum2) {
	let product = productNum1 * productNum2;
	return `${productNum1} times ${productNum2} is equal to ${product}.`
}

function quotientOfTwoNumbers(quotientNum1, quotientNum2) {
	let quotient = quotientNum1 / quotientNum2;
	return `${quotientNum1} divided by ${quotientNum2} is equal to ${quotient}.`
}

if (num1 + num2 < 10) {
	console.warn(sumOfTwoNumbers(num1, num2));
} else if (num1 + num2 >= 10 && num1 + num2 <= 19) {
	alert(differenceOfTwoNumbers(num1, num2));
} else if (num1 + num2 >= 20 && num1 + num2 <= 29) {
	alert(productOfTwoNumbers(num1, num2));
} else {
	alert(quotientOfTwoNumbers(num1, num2));
}

//------------------------------


let userName = prompt(`Enter your name:`);
let userAge = prompt(`Enter your age (please input a number):`);

if (userName == `` || userAge == `` || userName == null || userAge == null) {
	alert(`Are you a time traveler?`);
} else if (userName != `` && userAge != ``) {
	alert(`Hello ${userName}. You are ${userAge} years old.`); 
}

//------------------------------

function isLegalAge(age) {
	if (age >= 18) {
		return `You are of legal age.`
	} else {
		return `You are not allowed here.`
	}
}

alert(isLegalAge(userAge));

//------------------------------


switch (userAge) {
	case 18, `18`:
		alert(`You are now allowed to party.`);
		break;
	case 21, `21`:
		alert(`You are now part of the adult society.`);
		break;
	case 65, `65`:
		alert(`We thank you for your contribution to society.`);
		break;
	default:
		alert(`Are you sure you're not an alien?`);
		break;
}

//------------------------------


try {
	alert(isLegaaalAge(userAge));

}

catch(error) {
	console.warn(error.message);
}

finally {
	alert(isLegalAge(userAge));
}



















